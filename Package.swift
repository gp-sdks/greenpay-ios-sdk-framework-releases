// swift-tools-version:5.3
import PackageDescription
let package = Package(
    name: "GreenpaySDK",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "GreenpaySDK",
            targets: ["GreenpaySDK"])
    ],
    targets: [
        .binaryTarget(
            name: "GreenpaySDK",
            path: "GreenpaySDK.xcframework")
    ])