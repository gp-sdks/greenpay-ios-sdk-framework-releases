//
//  GreenpaySDK.h
//  GreenpaySDK
//
//  Created by Jesús Quirós on 7/3/20.
//  Copyright © 2020 Jesús Quirós. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDataCollector.h"
#import "NSData+SHA.h"

//! Project version number for GreenpaySDK.
FOUNDATION_EXPORT double GreenpaySDKVersionNumber;

//! Project version string for GreenpaySDK.
FOUNDATION_EXPORT const unsigned char GreenpaySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GreenpaySDK/PublicHeader.h>


